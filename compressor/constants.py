from enum import Enum, unique


class KEY(Enum):
    COUNTER = 'counter'
    ALL_URLS = 'urls'
    PREFIX = 'url'